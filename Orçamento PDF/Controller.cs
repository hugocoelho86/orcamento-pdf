﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orçamento_PDF
{
    class Controller
    {
        Model model;
        View view;

        public delegate void AtivacaoInterface(object origem);
        public event AtivacaoInterface AtivarInterface;

        public Controller()
        {
            view = new View(model);
            model = new Model(view);

            view.UtilizadorClicouEmCalcular += UtilizadorClicouEmCalcular;
        }
        public void IniciarPrograma()
        {
            view.AtivarInterface();
        }

        public void UtilizadorClicouEmDropBox(object fonte, System.EventArgs args)
        {
            model.ListaItens();
        }


        public void UtilizadorClicouEmCalcular(object fonte, System.EventArgs args)
        {
            model.CalcularOrcamento();
        }

    }



}

