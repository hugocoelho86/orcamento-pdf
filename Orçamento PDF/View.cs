﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orçamento_PDF
{
    public class View
    {

        private Model model;
        private Form1 janela;


        public event System.EventHandler UtilizadorClicouEmCalcular;

        public event System.EventHandler UtilizadorClicouEmDropBox;

        public delegate void SolicitaItens(ref List<Item> itensorcamento);
        public event SolicitaItens PrecisoListaItens;

        public delegate void SolicitaOrcamento (ref List<Item> itensorcamento);
        public event SolicitaOrcamento PrecisoItensOrc;

        internal View(Model m)
        {
            model = m;
        }

        public void AtivarInterface()
        {
            janela = new Form1();
            janela.View = this;

            janela.ShowDialog();
        }

        public void CliqueEmDropBox(object origem, EventArgs e)
        {
            UtilizadorClicouEmDropBox(origem, e);
        }

        public void  CliqueEmCalcular (object origem, EventArgs e)
        {
            UtilizadorClicouEmCalcular(origem, e);
        }

        void ListaItensDropBox()
        {
            List<Item> litens = new List<Item>();
            PrecisoListaItens(ref litens);
        }

        void DesenhaOrcamento()
        {
            List<Item> litens = new List<Item>();
            PrecisoItensOrc(ref litens);
        }

    }
}
